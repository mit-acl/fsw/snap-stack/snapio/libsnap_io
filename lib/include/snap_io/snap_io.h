/**
 * @file snap_io.h
 * @brief API for accessing ioboard via sDSP
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 April 2021
 */

#pragma once

#include <atomic>
#include <array>
#include <cstdint>
#include <mutex>
#include <thread>

namespace acl {
namespace snapio {

  /**
   * UART port to device mapping - note the differences between APQ8074 and APQ8096
   */
  enum class UART { /*APQ8074*/ J15 /*= 1*/, J13 /*= 2*/, J12 /*= 3*/, J9 /*= 4*/,
                    /*APQ8096*/ J7 /*= 9*/, J10 /*= 7*/, J11 /*= 12*/, /*J12 = 5*/ };

  class SnapIO
  {
  public:
    /**
     * @brief      SnapIO constructor.
     *
     *        The specified UART may be different depending if you are using
     *        the blue sf (APQ8074) or the red sfpro (or green voxl).
     *
     *        The baudrate should match that of the ioboard.
     *
     * @param[in]  uart      The UART (indicated by connector) to use
     * @param[in]  baudrate  Baudrate of serial connection
     */
    SnapIO(UART uart, int baudrate = 921600);
    ~SnapIO();

    /**
     * @brief      Sets the throttle value of each motor
     *
     * @param[in]  throttle   Throttle value in [0, 1] for each motor
     */
    void set_motors(const std::array<float, 6>& throttle);

    /**
     * @brief      Forwards an already packed buffer to ioboard via UART.
     *
     * @param[in]  data   buffer
     * @param[in]  bytes  buffer length
     */
    void forward_buffer(const uint8_t* data, int bytes);

  private:
    const int bus_; ///< UART (BLSP bus) to use
    const int baud_; ///< requested baudrate

    //\brief Threading
    std::atomic<bool> thread_stop_;
    std::mutex mtx_;
    std::thread read_thread_;

    /**
     * @brief      UART reader, called in a separate thread
     */
    void read_thread();

  };

} // ns snapio
} // ns acl
