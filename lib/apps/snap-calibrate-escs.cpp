/**
 * @file calibrate_esc.cpp
 * @brief Calibration tool for calibrating ESCs
 * @author Savva Morozov <savva@mit.edu>
 * @date 25 Dec 2019
 */

#include <cmath>
#include <chrono>
#include <csignal>
#include <cstring>
#include <iostream>
#include <sstream>
#include <thread>

#include <snap_io/snap_io.h>

// to be set by sighandler
volatile sig_atomic_t stop = 0;
void handle_sigint(int s) { stop = true; }

int main(int argc, char const *argv[])
{
  // install sig handler
  struct sigaction sa;
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_handler = handle_sigint;
  sa.sa_flags = 0; // not SA_RESTART
  sigaction(SIGINT, &sa, NULL);

  std::cout << std::endl;
  std::cout << "!! REMOVE THE PROPS !!" << std::endl;
  std::cout << std::endl;
  std::cout << "Use this tool to calibrate the throttle endpoints between flight" << std::endl;
  std::cout << "controller and ESCs - this is only necessary for PWM signalling." << std::endl;
  std::cout << std::endl;
  std::cout << "This tool will command 100% throttle, then after you connect power" << std::endl;
  std::cout << "to the ESCs, they *should* enter calibration mode (check datasheet)." << std::endl;
  std::cout << std::endl;
  std::cout << "Disconnect the ESCs. Press Enter to continue. ";
  std::cout << std::endl;
  std::cin.ignore();



  acl::snapio::SnapIO sio(acl::snapio::UART::J12, 921600);

  // command max throttle
  std::array<float, 6> throttle;
  throttle.fill(1.0f);
  sio.set_motors(throttle);

  std::cout << std::endl;
  std::cout << "ESCs are set to 100% throttle. Connect power to ESCs." << std::endl;
  std::cout << "You should hear 3 or 4 beeps indicating battery's cell count (3S, 4S)" << std::endl;
  std::cout << "and an ESC-specific beep squence indicating that max throttle is captured. " << std::endl;
  std::cout << "If not, something is wrong. If yes, press Enter to continue.";
  std::cin.ignore();

  // command min throttle
  throttle.fill(0.0f);
  sio.set_motors(throttle);

  std::cout << std::endl;
  std::cout << "ESCs are set to 0% throttle" << std::endl;
  std::cout << "You should hear ESC-specific beep sequence indicating that min throttle is captured." << std::endl;
  std::cout << "That's it! Calibration is complete. Test it out with snap-test-motors tool." << std::endl;
  std::cout << std::endl;

  return 0;
}
