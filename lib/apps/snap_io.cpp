/**
 * @file snapio.cpp
 * @brief API for accessing ioboard via sDSP
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 April 2021
 */

#include <chrono>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include <type_traits>

#include <voxl_io.h>

#include <sio_rpc.h>


#include "snap_io/snap_io.h"

#include "snapio-protocol/sio_serial.h"

namespace acl {
namespace snapio {

  //\brief Message handling
  static constexpr int BUF_LEN = 32 * 1024; ///< big 32k read buffer
  static constexpr int BUF_PKT_LEN = BUF_LEN / sizeof(sio_serial_message_t); ///< number of messages that fit
  sio_serial_message_t* msgs_buf__; ///< buffer for BUF_PKT_LEN messages read from parser

static int get_bus(UART uart)
{
  if (uart == UART::J7)  return 9;
  if (uart == UART::J10) return 7;
  if (uart == UART::J11) return 12;
  if (uart == UART::J12) return 5;

  throw std::runtime_error("Could not resolve UART device from port.");
  return -1;
}

SnapIO::SnapIO(UART uart, int baud)
: bus_(get_bus(uart)), baud_(baud)
{

  // initialize the RPC read buffer and sioparser
  msgs_buf__ = reinterpret_cast<sio_serial_message_t*>(voxl_rpc_shared_mem_alloc(BUF_LEN));
  if (msgs_buf__ == nullptr) {
    throw std::runtime_error("Error allocating shared memory buffers for sioparser");
  }

  if (sio_rpc_sioparser_init(bus_, baud_, BUF_LEN)) {
    std::stringstream ss;
    ss << "Error initializing sioparser UART (/dev/tty-" << bus_ << ") @ " << baud_ << " baud";
    throw std::runtime_error(ss.str());
  }

  // start reading in separate thread
  thread_stop_ = false;
  read_thread_ = std::thread(&SnapIO::read_thread, this);
}

// ----------------------------------------------------------------------------

SnapIO::~SnapIO()
{
  thread_stop_ = true;
  if (read_thread_.joinable()) {
    read_thread_.join();
  }

  sio_rpc_sioparser_close(bus_);
  voxl_rpc_shared_mem_free(reinterpret_cast<uint8_t*>(msgs_buf__));
}

// ----------------------------------------------------------------------------

void SnapIO::set_motors(const std::array<float, 6>& throttle)
{
  sio_serial_motorcmd_msg_t msg;

  // Convert motor throttle to PWM
  for (size_t i=0; i<throttle.size(); ++i) {
    if (throttle[i] >= 0 && throttle[i] <= 1.) {
      msg.throttle[i] = throttle[i];
    } else if (throttle[i] > 1) {
      msg.throttle[i] = 1.0f;
    } else {
      msg.throttle[i] = 0.0f;
    }
  }

  uint8_t buf[SIO_SERIAL_MAX_MESSAGE_LEN];
  const int len = sio_serial_motorcmd_msg_send_to_buffer(buf, &msg);

  forward_buffer(buf, len);
}

// ----------------------------------------------------------------------------

void SnapIO::forward_buffer(const uint8_t* data, int bytes)
{
  int bytes_written;
  if (sio_rpc_uart_write(bus_, data, bytes, &bytes_written)) {
    throw std::runtime_error("Error writing buffer");
  }

  if (bytes_written != bytes) {
    throw std::runtime_error("Error writing buffer: incorrect write length");
  }
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void SnapIO::read_thread()
{
  // this is a tight loop around the read function. Read will wait to return
  // untill there is either at least one message available or there was an error
  // this no need to sleep (hooray, no CPU busy-waiting!)
  while (!thread_stop_) {

    //  this blocks for up to 0.5 seconds waiting for a decoded message
    int msgs_read = 0;
    sio_rpc_sioparser_read(bus_, reinterpret_cast<uint8_t*>(msgs_buf__), BUF_LEN, &msgs_read);

    // buffer overrun
    if (msgs_read >= BUF_PKT_LEN) {
      std::cerr << "WARNING, sioparser buffer full!" << std::endl;
    }

    // other failure cases
    if (msgs_read < 0) {
      std::cerr << "ERROR exiting uart thread -- maybe someone else using uart?" << std::endl;
      thread_stop_ = true;
      return;
    }

    // handle received messages -- these should be fast!
    if (msgs_read > 0) {
      for (int i=0; i<msgs_read; i++) {
        switch (msgs_buf__[i].type) {
          case SIO_SERIAL_MSG_MOTORCMD:
          {
            sio_serial_motorcmd_msg_t msg;
            sio_serial_motorcmd_msg_unpack(&msg, &msgs_buf__[i]);
            // handle_motorcmd_msg(msg);
            break;
          }
        }
      }
    }

  }

}

} // ns snapio
} // ns acl
