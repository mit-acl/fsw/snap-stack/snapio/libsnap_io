/**
 * @file snap-test-pwm.cpp
 * @brief CLI tool to send pwm commands
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 Oct 2019
 */

#include <array>
#include <cmath>
#include <chrono>
#include <csignal>
#include <cstring>
#include <iostream>
#include <sstream>
#include <thread>

#include <snap_io/snap_io.h>

// to be set by sighandler
volatile sig_atomic_t stop = 0;
void handle_sigint(int s) { stop = true; }

static void _print_usage()
{
  std::cout << std::endl;
  std::cout << "Tool to send throttle values to motors." << std::endl;
  std::cout << std::endl;
  std::cout << "Example usage:" << std::endl;
  std::cout << std::endl;
  std::cout << "$ snap-test-motors 0        # set all motors to 0 throttle" << std::endl;
  std::cout << "$ snap-test-motors 0.1      # set all motors to 10% throttle" << std::endl;
  std::cout << "$ snap-test-motors 0 0.5 10 # ramp all motors from 0 to 50% throttle over 10 seconds" << std::endl;
  std::cout << std::endl;
}

int main(int argc, char const *argv[])
{
  // install sig handler
  struct sigaction sa;
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_handler = handle_sigint;
  sa.sa_flags = 0; // not SA_RESTART
  sigaction(SIGINT, &sa, NULL);

  if (argc < 2) {
    _print_usage();
    return -1;
  }

  // the parameters of the ramp (default is a constant-amplitude ramp)
  float thrlo = 0.0f;
  float thrhi = thrlo;
  float ramp_secs = 0.0f;

  // Send a constant throttle value
  if (argc == 2) {
    float tmp;
    std::istringstream ss(argv[1]);

    if (!(ss >> tmp)) {
      std::cerr << "Invalid number: " << argv[1] << std::endl;
    } else {
      thrlo = tmp;
      thrhi = tmp;
    }
  }

  // Send a throttle ramp
  if (argc == 4) {
    {
      float tmp;
      std::istringstream ss(argv[1]);
      if (!(ss >> tmp)) {
        std::cerr << "Invalid number: " << argv[2] << std::endl;
      } else {
        thrlo = tmp;
      }
    }

    {
      float tmp;
      std::istringstream ss(argv[2]);
      if (!(ss >> tmp)) {
        std::cerr << "Invalid number: " << argv[2] << std::endl;
      } else {
        thrhi = tmp;
      }
    }

    {
      float tmp;
      std::istringstream ss(argv[3]);
      if (!(ss >> tmp)) {
        std::cerr << "Invalid number: " << argv[3] << std::endl;
      } else {
        ramp_secs = std::fabs(tmp);
      }
    }
  }

  acl::snapio::SnapIO sio(acl::snapio::UART::J12, 921600);

  if (ramp_secs == 0.0f) {
    // constant throttle value
    std::cout << "Setting throttle to " << thrlo << std::endl;

    while (!stop) {

      // command min throttle
      std::array<float, 6> throttle;
      throttle.fill(thrlo);
      sio.set_motors(throttle);

      // avoid busy waiting
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  } else {
    // throttle ramp
    std::cout << "Ramping throttle from " << thrlo  << " to ";
    std::cout << thrhi << " in " << ramp_secs << " seconds";
    std::cout << std::endl;

    // sampling period (in milliseconds)
    constexpr int Tms = 50;

    // discretization based on user input
    const int N = std::round(ramp_secs / (Tms*1e-3));
    const float step = (thrhi - thrlo) / N;

    std::cout << "Discretization: " << N << " steps of " << step << std::endl;

    // throttle command
    std::array<float, 6> throttle;
    throttle.fill(0.0);
    float thrcur = thrlo;

    // ramp until high
    for (; thrcur<thrhi; thrcur+=step) {
      throttle.fill(thrcur);
      sio.set_motors(throttle);
      std::cout << "throttle: " << thrcur << std::endl;

      std::this_thread::sleep_for(std::chrono::milliseconds(Tms));
    }

    while (!stop) {
      sio.set_motors(throttle);
      // avoid busy waiting
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

  }

  return 0;
}
