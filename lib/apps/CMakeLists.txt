# CMakeLists for the apps-proc side code for libsnap_io
cmake_minimum_required(VERSION 3.0)

set(CMAKE_CXX_FLAGS "-std=c++11 -g ${CMAKE_CXX_FLAGS}")

# Set a local variables
set(LIBNAME snap_io)
set(IDLNAME sio_rpc)
set(QC_SOC_TARGET APQ8096)

# general compile flags
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0 -g -std=gnu99 -fno-strict-aliasing \
                    -fdata-sections -fno-zero-initialized-in-bss -Wall -Wextra \
                    -Werror -Wno-unused-parameter -Wno-unused-function \
                    -Wno-unused-variable -Wno-gnu-array-member-paren-init \
                    -Wno-cast-align -Wno-missing-braces -Wno-strict-aliasing \
                    -mfloat-abi=softfp")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O0 -g -fno-strict-aliasing \
                    -fdata-sections -fno-zero-initialized-in-bss -Wall -Wextra \
                    -Werror -Wno-unused-parameter -Wno-unused-function \
                    -Wno-unused-variable -Wno-gnu-array-member-paren-init \
                    -Wno-cast-align -Wno-missing-braces -Wno-strict-aliasing \
                    -mfloat-abi=softfp")

# include the cmake_hexagon macro directory and the linux_app macros
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} $ENV{CMAKE_HEXAGON_DIR})
include(linux_app)

# generate stubs from idl file, sdsp code has to do the same
FASTRPC_STUB_GEN(../idl/${IDLNAME}.idl)

# all source files, library and examples, will need the headers for this library
include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/../include/
)

# cmake_hexagon macro to make a linux lib
LINUX_LIB(
    LIB_NAME ${LIBNAME}
    IDL_NAME ${IDLNAME}
    SOURCES  snap_io.cpp
)

target_compile_definitions(${LIBNAME} PUBLIC APQ8096)

# Search these paths for headers, but do it last since we shouldn't use these
# system headers (we are cross compiling). But, we need voxl_io.h. Once CMake
# ver in voxl-hexagon is updated, use target_include_directories(.. AFTER ..)
# target_include_directories(${LIBNAME} AFTER PRIVATE /usr/include)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -idirafter /usr/include")

add_library(voxl_io SHARED IMPORTED GLOBAL)
set_property(TARGET voxl_io PROPERTY IMPORTED_LOCATION /usr/lib/libvoxl_io.so)

add_executable(snap-test-motors snap-test-motors.cpp)
target_link_libraries(snap-test-motors snap_io voxl_io)

add_executable(snap-calibrate-escs snap-calibrate-escs.cpp)
target_link_libraries(snap-calibrate-escs snap_io voxl_io)

# # find all public header files to install in include directory
# file(GLOB LIB_HEADERS ${CMAKE_CURRENT_SOURCE_DIR}/../include/*.h)
# set_target_properties(${LIBNAME} PROPERTIES PUBLIC_HEADER "${LIB_HEADERS}")



install(
    TARGETS ${LIBNAME} snap-test-motors snap-calibrate-escs
    LIBRARY DESTINATION /usr/lib
    RUNTIME DESTINATION /usr/bin
    # PUBLIC_HEADER DESTINATION /usr/include
)

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../include/ DESTINATION /usr/include)