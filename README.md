# libsnap_io

Userspace library for interfacing with snap-stack [`ioboard`](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/ioboard) via the Sensors DSP (sdsp).

## Build Instructions

1. Requires the voxl-hexagon docker image to build. Follow the [voxl-docker instructions](https://gitlab.com/voxl-public/voxl-docker)
2. Pull in the [`snapio-protocol`](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/snapio-protocol) git submodule

    ```bash
    git submodule update --init --recursive
    ```
3. Launch the voxl-hexagon docker image in the root directory of this repo

    ```bash
    ~/libsnap_io $ voxl-docker -i voxl-hexagon
    voxl-hexagon:~$
    ````
4. Run build.sh inside the docker

    ```bash
    voxl-hexagon:~$ ./build.sh
    ````

## Installation

1. Generate an ipk package inside the docker.

    ```bash
    voxl-hexagon:~$ ./make_package.sh
    ````
2. You can now push the ipk package to the VOXL and install with opkg however you like. To do this over ADB, you may use the included helper script: install_on_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

    ```bash
    ~/libsnap_io$ ./install_on_voxl.sh
    ```

## Hardware Connection

Use a [Teensy 4.0](https://www.pjrc.com/store/teensy40.html), flashed with the latest [`ioboard`](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/ioboard) firmware. The teensy should connect to motors as specified in the [`PWM_PINS`](https://gitlab.com/mit-acl/fsw/snap-stack/snapio/ioboard/-/blob/main/firmware/firmware.ino#L17) array. Connect the Teensy's UART1 to one of the ports on the sfpro, e.g., J12 (by the power connector).